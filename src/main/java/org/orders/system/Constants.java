package org.orders.system;

public final class Constants {
    private Constants() {
    }

    public static final String EGG = "Eggs";
    public static final String TOAST = "Toast";
    public static final String COFFEE ="Coffee";

    public static final String SALAD = "Salad";
    public static final String CHIPS = "Chips";
    public static final String SODA ="Soda";

    public static final String STEAK = "Steak";
    public static final String POTATOES = "Potatoes";
    public static final String WINE = "Wine";
    public static final String CAKE = "Cake";

    public static final String WATER = "Water";

    public static final String COMMA = ", ";
    public static final String OPENPARENTHESIS = "(";
    public static final String CLOSEPARENTHESIS = ")";
    public static final String PATH ="result\\test.txt";
}
