package org.orders.system;

import java.util.Collection;

public interface Meal {
      String displayMap(Collection<Integer> ids) ;
}
