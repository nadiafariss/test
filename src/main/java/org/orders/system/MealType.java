package org.orders.system;

public enum MealType {
    BREAKFAST,
    LUNCH,
    DINNER
}

